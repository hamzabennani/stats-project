package com.xebia.fnag.stats.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Class de configuration Spring
 * 
 * @author abbanabennanih
 *
 */
@Configuration
@ComponentScan("com.xebia.fnag.stats")
public class ContextConfig {

}
