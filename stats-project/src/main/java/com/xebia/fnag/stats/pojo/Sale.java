package com.xebia.fnag.stats.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Représente une vente
 * 
 * @author abbanabennanih
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Sale {

	private Seller seller;

	private Product product;

	private int quantity;

}
