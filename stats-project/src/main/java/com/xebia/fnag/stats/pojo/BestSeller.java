package com.xebia.fnag.stats.pojo;

import java.util.List;
import java.util.Locale;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Représente le(s) meilleur(s) vendeur(s)
 * 
 * @author abbanabennanih
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class BestSeller {

	private List<Seller> sellers;

	private float totalSales;

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Seller seller : sellers) {
			sb.append(
					String.format(Locale.US, "TOPSELLER|%s|%s|%.2f\n", seller.getShop(), seller.getName(), totalSales));
		}
		return sb.toString();
	}

}
