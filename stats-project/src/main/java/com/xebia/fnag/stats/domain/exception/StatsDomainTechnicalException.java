package com.xebia.fnag.stats.domain.exception;

/**
 * Domain exception
 * 
 * @author abbanabennanih
 *
 */
public class StatsDomainTechnicalException extends Exception {

	private static final long serialVersionUID = -23923824020419282L;

	public StatsDomainTechnicalException() {
		super();
	}

	public StatsDomainTechnicalException(String message) {
		super(message);
	}

	public StatsDomainTechnicalException(Throwable cause) {
		super(cause);
	}

	public StatsDomainTechnicalException(String message, Throwable cause) {
		super(message, cause);
	}

}
