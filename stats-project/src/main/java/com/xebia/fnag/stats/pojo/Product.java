package com.xebia.fnag.stats.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Permet de d�crire un produit
 * 
 * @author abbanabennanih
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Product {

	private String reference;

	private float price;

	private String description;

}
