package com.xebia.fnag.stats;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.xebia.fnag.stats.config.ContextConfig;
import com.xebia.fnag.stats.domain.StatisticsDataLoaderDomain;
import com.xebia.fnag.stats.domain.StatisticsDomain;
import com.xebia.fnag.stats.domain.exception.StatsDomainFunctionalException;
import com.xebia.fnag.stats.domain.exception.StatsDomainTechnicalException;
import com.xebia.fnag.stats.pojo.BestSeller;
import com.xebia.fnag.stats.pojo.Sale;
import com.xebia.fnag.stats.pojo.TopSale;

import lombok.extern.slf4j.Slf4j;

/**
 * Class permettant de lancer le traitement
 * 
 * @author abbanabennanih
 *
 */
@Slf4j
public class Main {

	private static List<Sale> sales;

	private static StatisticsDomain statisticsDomain;

	private static StatisticsDataLoaderDomain dataLoader;

	public static void main(String[] args) {

		try {

			Main.loadAppContext();

			Main.loadData(args);

			Main.getTopSale();

			Main.getBestSeller();

		} catch (StatsDomainFunctionalException e) {
			log.error(e.getMessage(), e);
		} catch (StatsDomainTechnicalException e) {
			log.error(e.getMessage(), e);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}

	/**
	 * Permet de charger le contexte applicatif
	 */
	private static void loadAppContext() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(ContextConfig.class);

		statisticsDomain = ctx.getBean(StatisticsDomain.class);

		dataLoader = ctx.getBean(StatisticsDataLoaderDomain.class);

		ctx.close();

	}

	/**
	 * Permet de lancer le traitement de calcul du produit le plus vendu
	 */
	private static void getTopSale() {

		TopSale topSale = statisticsDomain.getTopSale(sales);

		log.info(topSale.toString());
	}

	/**
	 * Permet de lancer le traitement de calcul du meilleur vendeur
	 */
	private static void getBestSeller() {

		BestSeller bestSeller = statisticsDomain.getBestSeller(sales);

		log.info(bestSeller.toString());
	}

	/**
	 * Permet de lancer le traitement de chargement des donn�es
	 * 
	 * @param filePath
	 *            : chemin du fichier contennant les donn�es d'entr�e
	 * @throws StatsDomainTechnicalException
	 */
	private static void loadData(String[] args) throws StatsDomainFunctionalException, StatsDomainTechnicalException {
		if (args == null || args.length == 0) {
			throw new StatsDomainFunctionalException("Veuillez servir le chemin du fichier d'entr�e en param�tre.");
		}
		sales = dataLoader.loadData(args[0]);
	}

}
