package com.xebia.fnag.stats.domain.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.xebia.fnag.stats.domain.StatisticsDomain;
import com.xebia.fnag.stats.pojo.BestSeller;
import com.xebia.fnag.stats.pojo.Product;
import com.xebia.fnag.stats.pojo.Sale;
import com.xebia.fnag.stats.pojo.Seller;
import com.xebia.fnag.stats.pojo.TopSale;

/**
 * Impl�mentation par d�faut du traitement m�tier des statistiques
 * 
 * @author abbanabennanih
 *
 */
@Service
public class DefaultStatistisDomain implements StatisticsDomain {

	/**
	 * {@inheritDoc}
	 */
	public TopSale getTopSale(List<Sale> sales) {
		TopSale topSale = new TopSale();
		topSale.setProducats(new ArrayList<Product>());

		// Compte des ventes des produits
		Map<Product, Integer> salesCountMap = new HashMap<Product, Integer>();
		for (Sale sale : sales) {
			int productCount = sale.getQuantity();
			Product product = sale.getProduct();
			if (salesCountMap.containsKey(product)) {
				productCount += salesCountMap.get(product).intValue();
			}
			salesCountMap.put(sale.getProduct(), productCount);
		}

		// Identification des produits les plus vendus
		int maxCount = 0;
		for (Map.Entry<Product, Integer> entry : salesCountMap.entrySet()) {
			if (entry.getValue() > maxCount) {
				maxCount = entry.getValue();
			}
		}
		topSale.setCount(maxCount);
		for (Map.Entry<Product, Integer> entry : salesCountMap.entrySet()) {
			if (maxCount == entry.getValue()) {
				topSale.getProducats().add(entry.getKey());
			}
		}

		return topSale;
	}

	/**
	 * {@inheritDoc}
	 */
	public BestSeller getBestSeller(List<Sale> sales) {

		BestSeller bestSeller = new BestSeller();
		bestSeller.setSellers(new ArrayList<Seller>());

		// Compte du montant total des ventes de chaque vendeur
		Map<Seller, Float> salesPriceMap = new HashMap<Seller, Float>();
		for (Sale sale : sales) {
			Seller seller = sale.getSeller();
			float totalPrice = sale.getProduct().getPrice() * sale.getQuantity();
			if (salesPriceMap.containsKey(seller)) {
				totalPrice = salesPriceMap.get(seller).longValue() + totalPrice;
			}
			salesPriceMap.put(sale.getSeller(), totalPrice);
		}

		// Identification des vendeurs avec le montant total des ventes le plus
		// grand
		float maxPrice = 0.0f;
		for (Map.Entry<Seller, Float> entry : salesPriceMap.entrySet()) {
			if (entry.getValue() > maxPrice) {
				maxPrice = entry.getValue();
			}
		}
		bestSeller.setTotalSales(maxPrice);
		for (Map.Entry<Seller, Float> entry : salesPriceMap.entrySet()) {
			if (maxPrice == entry.getValue()) {
				bestSeller.getSellers().add(entry.getKey());
			}
		}

		return bestSeller;
	}

}
