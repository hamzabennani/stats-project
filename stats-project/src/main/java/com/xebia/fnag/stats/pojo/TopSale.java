package com.xebia.fnag.stats.pojo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Représente le(s) produit(s) les plus vendus
 * 
 * @author abbanabennanih
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TopSale {

	private List<Product> producats;

	private int count;

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Product product : producats) {
			sb.append(String.format("TOPSALE|%s|%s|%d\n", product.getReference(), product.getDescription(), count));
		}
		return sb.toString();
	}

}
