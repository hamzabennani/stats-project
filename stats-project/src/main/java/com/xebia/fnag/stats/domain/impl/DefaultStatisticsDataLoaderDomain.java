package com.xebia.fnag.stats.domain.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.xebia.fnag.stats.domain.StatisticsDataLoaderDomain;
import com.xebia.fnag.stats.domain.exception.StatsDomainTechnicalException;
import com.xebia.fnag.stats.pojo.Product;
import com.xebia.fnag.stats.pojo.Sale;
import com.xebia.fnag.stats.pojo.Seller;

/**
 * Impl�mentation par d�faut du traitement de chargement des donn�es d'entr�e
 * 
 * @author abbanabennanih
 *
 */
@Service
public class DefaultStatisticsDataLoaderDomain implements StatisticsDataLoaderDomain {

	private Map<String, Product> products = new HashMap<String, Product>();

	/**
	 * {@inheritDoc}
	 */
	public List<Sale> loadData(String filePath) throws StatsDomainTechnicalException {

		File file = new File(filePath);
		FileInputStream fileInputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileInputStream = new FileInputStream(file);

			inputStreamReader = new InputStreamReader(fileInputStream);

			bufferedReader = new BufferedReader(inputStreamReader);

			int productsCount = Integer.parseInt(bufferedReader.readLine());

			this.loadProductsData(productsCount, bufferedReader);

			int saleCount = Integer.parseInt(bufferedReader.readLine());

			return this.loadSalesData(saleCount, bufferedReader);

		} catch (FileNotFoundException e) {
			throw new StatsDomainTechnicalException("Le chemin du fichier est introuvable", e);
		} catch (NumberFormatException e) {
			throw new StatsDomainTechnicalException(
					"Une erreur s'est produite lors de la conversion du nombre des produits / ventes", e);
		} catch (IOException e) {
			throw new StatsDomainTechnicalException(
					"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
		} catch (Exception e) {
			throw new StatsDomainTechnicalException(
					"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					throw new StatsDomainTechnicalException(
							"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
				}
			}
			if (inputStreamReader != null) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					throw new StatsDomainTechnicalException(
							"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
				}
			}
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					throw new StatsDomainTechnicalException(
							"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
				}
			}
		}

	}

	/**
	 * Permet de charger les donn�es relatives aux ventes
	 * 
	 * @param saleCount
	 *            : nombre de ventes
	 * @param bufferedReader
	 * @return
	 * @throws StatsDomainTechnicalException
	 */
	private List<Sale> loadSalesData(int saleCount, BufferedReader bufferedReader) throws StatsDomainTechnicalException {

		List<Sale> sales = new ArrayList<Sale>();

		for (int i = 0; i < saleCount; i++) {
			String[] values = this.readLineValues(bufferedReader);

			Sale sale =
					new Sale(new Seller(values[0], values[1]), products.get(values[2]), Integer.parseInt(values[3]));

			sales.add(sale);

		}

		return sales;
	}

	/**
	 * Permet de charger les donn�es relatives aux produits
	 * 
	 * @param productsCount
	 *            : nombre de produits
	 * @param bufferedReader
	 * @throws StatsDomainTechnicalException
	 */
	private void loadProductsData(int productsCount, BufferedReader bufferedReader) throws StatsDomainTechnicalException {
		for (int i = 0; i < productsCount; i++) {
			String[] values = this.readLineValues(bufferedReader);

			Product product = new Product(values[0], Float.parseFloat(values[1]), values[2]);

			products.put(product.getReference(), product);
		}
	}

	/**
	 * Permet de lire la valeurs d'une lignes
	 * 
	 * @param bufferedReader
	 * @return
	 * @throws StatsDomainTechnicalException
	 */
	private String[] readLineValues(BufferedReader bufferedReader) throws StatsDomainTechnicalException {
		try {
			String line = bufferedReader.readLine();
			return line.split("\\|");
		} catch (IOException e) {
			throw new StatsDomainTechnicalException(
					"Une erreur s'est produite lors du chargement des donn�es depuis le fichier d'entr�e", e);
		}

	}

}
