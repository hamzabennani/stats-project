package com.xebia.fnag.stats.domain;

import java.util.List;

import com.xebia.fnag.stats.domain.exception.StatsDomainTechnicalException;
import com.xebia.fnag.stats.pojo.Sale;

/**
 * D�fini les services de traitement de chargement des donn�es d'entr�e
 * 
 * @author abbanabennanih
 *
 */
public interface StatisticsDataLoaderDomain {

	/**
	 * Permet de charger les donn�es des ventes de produits
	 * 
	 * @param filePath
	 *            : chemin du fichier des donn�es
	 * @return List<Sale>
	 */
	public List<Sale> loadData(String filePath) throws StatsDomainTechnicalException;

}
