package com.xebia.fnag.stats.domain.exception;

/**
 * Domain exception
 * 
 * @author abbanabennanih
 *
 */
public class StatsDomainFunctionalException extends Exception {

	private static final long serialVersionUID = -23923824020419282L;

	public StatsDomainFunctionalException() {
		super();
	}

	public StatsDomainFunctionalException(String message) {
		super(message);
	}

	public StatsDomainFunctionalException(Throwable cause) {
		super(cause);
	}

	public StatsDomainFunctionalException(String message, Throwable cause) {
		super(message, cause);
	}

}
