package com.xebia.fnag.stats.domain;

import java.util.List;

import com.xebia.fnag.stats.pojo.BestSeller;
import com.xebia.fnag.stats.pojo.Sale;
import com.xebia.fnag.stats.pojo.TopSale;

/**
 * traitement m�tier des statistiques
 * 
 * @author abbanabennanih
 *
 */
public interface StatisticsDomain {

	/**
	 * Permet d'identifier le produit le plus vendu
	 * 
	 * @param sales
	 *            : liste des ventes
	 * 
	 * @return le produit le plus vendu
	 */
	public TopSale getTopSale(List<Sale> sales);

	/**
	 * Permet d'indentifier le meilleur vendeur
	 * 
	 * @param sales
	 *            : liste des ventes
	 * 
	 * @return le vendueur le plus vendu
	 */
	public BestSeller getBestSeller(List<Sale> sales);

}
