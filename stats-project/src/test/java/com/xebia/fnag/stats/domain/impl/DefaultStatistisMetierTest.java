package com.xebia.fnag.stats.domain.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xebia.fnag.stats.config.ContextConfig;
import com.xebia.fnag.stats.domain.StatisticsDomain;
import com.xebia.fnag.stats.pojo.BestSeller;
import com.xebia.fnag.stats.pojo.Product;
import com.xebia.fnag.stats.pojo.Sale;
import com.xebia.fnag.stats.pojo.Seller;
import com.xebia.fnag.stats.pojo.TopSale;

/**
 * Class de test de {@link DefaultStatistisDomain}
 * 
 * @author abbanabennanih
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ContextConfig.class })
public class DefaultStatistisMetierTest {

	@Inject
	private StatisticsDomain statisticsMetier;

	/**
	 * Permet de tester le traitement d'identification du produit le plus vendu
	 */
	@Test
	public void testTopSale() {

		TopSale topSale = statisticsMetier.getTopSale(this.getSales());

		Assert.assertNotNull(topSale);
		Assert.assertEquals("TOPSALE|T127|T-shirt 'no place like 127.0.0.1'|4\n", topSale.toString());

	}

	/**
	 * Permet de tester le traitement d'indentification du meilleur vendeur
	 * 
	 */
	@Test
	public void testBestSeller() {
		BestSeller bestSeller = statisticsMetier.getBestSeller(this.getSales());

		Assert.assertNotNull(bestSeller);
		Assert.assertEquals("TOPSELLER|Lyon|Alice|229.98\n", bestSeller.toString());
	}

	/**
	 * Permet de construire une liste de ventes
	 * 
	 * @return List<Sale>
	 */
	private List<Sale> getSales() {
		List<Sale> sales = new ArrayList<Sale>();

		Seller bob = new Seller("Bob", "Paris");
		Product lmusb = new Product("LMUSB", 20f, "Lance-missile USB");
		sales.add(new Sale(bob, lmusb, 1));

		Seller alice = new Seller("Alice", "Lyon");
		Product mkb = new Product("MKB", 200f, "Clavier m�canique");
		sales.add(new Sale(alice, mkb, 1));

		Product t127 = new Product("T127", 14.99f, "T-shirt 'no place like 127.0.0.1'");
		sales.add(new Sale(alice, t127, 2));

		sales.add(new Sale(bob, t127, 1));

		Seller chunck = new Seller("Chunck", "Paris");
		sales.add(new Sale(chunck, t127, 1));

		return sales;
	}

}
